package com.techu.apitechuv2.services;


import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers");
        List<UserModel> result;
        if (orderBy!=null){
            System.out.println("Ordenación");
            result = this.userRepository.findAll(Sort.by("age"));
        }else {
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> getUserById (String id){

        System.out.println("getUserById");
        System.out.println("Obteniendo usuario con id: "+ id);

        return this.userRepository.findById(id);
    }

    public UserModel createUser(UserModel user){
        System.out.println("add");
        return this.userRepository.insert(user);
    }

    public UserModel updateUser (UserModel user){
        System.out.println("updateUser");

        //buscar por id
        Optional<UserModel> sUser = this.getUserById(user.getId());

        if (sUser.isPresent()) {
            System.out.println("Usuario encontrado");
            if (user.getName() == null) {
                System.out.println("No se actualiza nombre");
                user.setName(sUser.get().getName());
            }
            if (user.getAge() <= 0) {
                System.out.println("No se actualiza edad.");
                user.setAge(sUser.get().getAge());
            }
            return this.userRepository.save(user);
        }else return new UserModel();

    }

    public boolean deleteUser(String id) {
        System.out.println("delete");

        if(this.getUserById(id).isPresent()){
            System.out.println("User encontrado");
            this.userRepository.delete(this.getUserById(id).get());
            System.out.println("User borrado");
            return true;
        }
        return false;
    }
}
