package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sound.midi.Soundbank;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll");
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById (String id){

        System.out.println("findById");
        System.out.println("Obteniendo producto con id: "+ id);

        //devuelve un Optional(java 8)
        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product){
        System.out.println("add");
        return this.productRepository.insert(product);
    }

    public ProductModel update (ProductModel product){
        System.out.println("update");
        return this.productRepository.save(product);
    }

    public void delete(ProductModel product) {
        System.out.println("delete");
        this.productRepository.delete(product);
    }

}
