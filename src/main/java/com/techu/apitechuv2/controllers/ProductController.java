package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.ProductService;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2") //indicamos la ruta base para el controlador
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return this.productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById (@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("El id del producto a buscar es : " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        if(result.isPresent()){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }else {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    /*
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    */

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct (@RequestBody ProductModel newProduct){

        System.out.println("addProduct");
        System.out.println("El id de nuevo producto es: " + newProduct.getId());
        System.out.println("La descripcion de nuevo producto es: " + newProduct.getDesc());
        System.out.println("El precio de nuevo producto es: " + newProduct.getPrice());

        //Devolvemos un responseEntity con el nuevo producto y un 201 de respuesta (Created)
        return new ResponseEntity<>(this.productService.add(newProduct), HttpStatus.CREATED);
    }


    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct (@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("El id de producto es: " + id);
        System.out.println("La descripcion de producto es: " + product.getDesc());
        System.out.println("El precio de nuevo producto es: " + product.getPrice());

        //buscar por id
        Optional<ProductModel> sProduct = this.productService.findById(id);

        if (sProduct.isPresent()){
            System.out.println("Producto encontrado");
            product.setId(id);
            if (product.getDesc()== null) {
                System.out.println("No se actualiza descripción");
                product.setDesc(sProduct.get().getDesc());
            }
            if (product.getPrice()==0) {
                System.out.println("No se actualiza el precio.");
                product.setPrice(sProduct.get().getPrice());
            }
            this.productService.add(product);
            System.out.println("Producto actualizado");
            return new ResponseEntity(product,HttpStatus.OK);
        }

        System.out.println("Producto no encontrado");
        return new ResponseEntity("Producto no encontrado",HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("El id de pathParam es: " + id);

        //buscar por id. Podemos tener esta lógica en el servicio en lugar del controlador (el delete puede devolver un boolean y sólo controlar la respuesta)
        Optional<ProductModel> sProduct = this.productService.findById(id);

        if (sProduct.isPresent()) {
            System.out.println("Producto encontrado");
            this.productService.delete(sProduct.get());
            System.out.println("Producto borrado");
            return new ResponseEntity("Producto borrado.", HttpStatus.OK);
        }

        System.out.println("Producto no encontrado");
        return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
    }

}
