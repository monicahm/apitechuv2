package com.techu.apitechuv2.controllers;


import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers (@RequestParam(name = "$orderby",required = false) String orderBy){
        System.out.println("getUsers");
        String order = "age"; //ordenación permitida
        System.out.println("query param: " + orderBy);

        return new ResponseEntity<>(this.userService.getUsers(orderBy),HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById (@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("El id del user a buscar es : " + id);

        Optional<UserModel> result = this.userService.getUserById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser (@RequestBody UserModel newUser){

        System.out.println("addUser");
        System.out.println("El id de nuevo usuario es: " + newUser.getId());
        System.out.println("El nombre de nuevo usuario: " + newUser.getName());
        System.out.println("La edad de nuevo usuario es: " + newUser.getAge());

        return new ResponseEntity<UserModel>(this.userService.createUser(newUser), HttpStatus.CREATED);
    }


    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser (@RequestBody UserModel user, @PathVariable String id){

        System.out.println("updateUser");
        System.out.println("El id de usuario es: " + id);
        System.out.println("Nombre de usuario es: " + user.getName());
        System.out.println("Edad de usuario es: " + user.getAge());

        //buscar por id
        Optional<UserModel> sUser = this.userService.getUserById(id);

        if (sUser.isPresent()){
            System.out.println("Usuario encontrado");
            user.setId(id);
            if (user.getName()== null) {
                System.out.println("No se actualiza nombre");
                user.setName(sUser.get().getName());
            }
            if (user.getAge()<=0) {
                System.out.println("No se actualiza edad.");
                user.setAge(sUser.get().getAge());
            }
            this.userService.updateUser(user);
            System.out.println("Usuario actualizado");
            return new ResponseEntity(user,HttpStatus.OK);
        }

        System.out.println("Usuario no encontrado");
        return new ResponseEntity("Usuario no encontrado",HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("El id de pathParam es: " + id);

        boolean borrado = this.userService.deleteUser(id);
        return new ResponseEntity<>(
                borrado ? "Usuario borrado": "Usuario no encontrado",
                borrado ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
        }

}
